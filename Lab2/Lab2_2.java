package Lab2;

/**
 * Created by chernikov on 4/8/2016.
 */
public class Lab2_2 {

    public static void main (String [] args){
        System.out.println("Переводим каждый примитивный тип в строку:");
        System.out.println();
        //byte
        byte a = 100;
        String b = Byte.toString(a);
        System.out.println("Теперь это не тип, а строка: " + b);
        //short
        short c = 5555;
        String d = Short.toString(c);
        System.out.println("Теперь это не тип, а строка: " + d);
        //int
        int e = 12345;
        String f = Integer.toString(e);
        System.out.println("Теперь это не тип, а строка: " + f);
        //long
        long g = 10L;
        String h = Long.toString(g);
        System.out.println("Теперь это не тип, а строка: " + h);
        //float
        float i = 3.14F;
        String j = Float.toString(i);
        System.out.println("Теперь это не тип, а строка: " + j);
        //double
        double k = 111.1;
        String l = Double.toString(k);
        System.out.println("Теперь это не тип, а строка: " + l);
        //boolean
        boolean m = true;
        String n = Boolean.toString(m);
        System.out.println("Теперь это не тип, а строка: " + n);
        //char
        char o = 653;
        String p = Character.toString(o);
        System.out.println("Теперь это не тип, а строка: " + p);

        System.out.println();

        System.out.println("Переводим полученную строку обратно в примитивный тип:");
        System.out.println();
        //byte
        Byte a1 = Byte.valueOf(b);
        System.out.println("Теперь это не строка, а тип " + a1);
        //short
        Short c1 = Short.valueOf(d);
        System.out.println("Теперь это не строка, а тип " + c1);
        //int
        Integer e1 = Integer.valueOf(f);
        System.out.println("Теперь это не строка, а тип " + e1);
        //long
        Long g1 = Long.valueOf(h);
        System.out.println("Теперь это не строка, а тип " + g1);
        //float
        Float i1 = Float.valueOf(j);
        System.out.println("Теперь это не строка, а тип " + i1);
        //double
        Double k1 = Double.valueOf(l);
        System.out.println("Теперь это не строка, а тип " + k1);
        //boolean
        Boolean m1 = Boolean.valueOf(n);
        System.out.println("Теперь это не строка, а тип " + m1);
        //char
        char o1 = p.charAt(0);
        System.out.println("Теперь это не строка, а тип " + o1);
    }
}
