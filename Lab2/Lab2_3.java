package Lab2;

/**
 * Created by chernikov on 4/8/2016.
 */
public class Lab2_3 {

    public static void main (String [] args){
        System.out.println("Переведим число с точкой в тип без точки:");
        double d = 3.1415;
        int i = (int) d;
        System.out.println("3.1415 -> " + i);

        System.out.println("Производим обратное действие:");
        double a = (double) i;
        System.out.println("3 -> " + a);
    }
}
