package Lab2;

/**
 * Created by Cronin on 10.04.2016.
 */
public class Lab2_1 {

    public static void main (String [] args){

        System.out.println("Арифметические операторы (=, +, -, *, /, %):");

        int a = 1 + 1;
        int b = a * 3;
        int c = b / 2;
        int d = b - a;
        int e = -d;
        int f = 52;
        System.out.println("a = 1 + 1 = " + a);
        System.out.println("b = a * 3  = " + b);
        System.out.println("c = b / 2  = " + c);
        System.out.println("d = b - a  = " + d);
        System.out.println("e = -d = " + e);
        System.out.println("f = 52 % 10 = " + f % 10);

        System.out.println("Логические операторы (||, &&, !):");

        boolean g = true;
        boolean h = false;
        boolean i = g || h;
        boolean j = g && h;
        boolean k = !g;
        System.out.println(" g = " + g);
        System.out.println(" h = " + h);
        System.out.println(" g || h = " + i);
        System.out.println(" g && h = " + j);
        System.out.println(" !g = " + k);

        System.out.println("Операторы сравнения (>, <, >=, <=, !=, ==):");

        int l = 5;
        int m = 10;
        boolean n = l < m;
        boolean o = l > m;
        boolean p = (l == m);
        boolean q = (l != m);
        System.out.println(" n = l < m = " + n);
        System.out.println(" o = l > m = " + o);
        System.out.println(" p = l == m = " + p);
        System.out.println(" q = l != m = " + q);

        }
}
