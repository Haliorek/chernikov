package Lab3;

import java.util.Scanner;

/**
 * Created by chernikov on 4/13/2016.
 */
public class Lab3_5 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Вводите через Enter любые целые числа, затем введите слово (сумма), что бы вывести на экран их сумму:");
        int a = 0;

        while (true) {
            String b = sc.next();
            try {
                if (b.equals("сумма"))
                    break;
                else
                    a += Integer.parseInt(b);
                System.out.println("Введите следующее число либо слово (сумма): ");
            }
            catch (Exception e) {
                System.out.println("Вы ввели недопустимый символ! Введите число либо слово (сумма): ");
            }
        }
        System.out.println("Сумма введенных чисел равна: " + a);
    }
}
