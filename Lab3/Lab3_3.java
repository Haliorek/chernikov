package Lab3;

import java.util.Scanner;

/**
 * Created by chernikov on 4/11/2016.
 */
public class Lab3_3 {

    public static void main(String[] args) {

        boolean g;
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите любое число N и система выведет на экран все простые числа в диапозоне от 1 до N:");
        int n = sc.nextInt();

        for (int i = 2; i <= n; i++) {
            g = true;
            if (i!=2 && i%2==0)
                continue;
            for (int h = 3; h*h <= i; h+=2) {
                if (i % h == 0){
                    g = false;
                    break;
                }
            }
            if (g) {
                System.out.println(i);
        }
        }
    }
    }